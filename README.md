# Chat en temps réél avec Nodejs, Express & MongoDB

## Procédure d'installation

```
git clone
cd realtime-chat
npm install
```
### Créer la base de données et la collection 'messages' (à faire 1 fois au démarrage)

```
node db-connect.js
```

### Lancer le server avec nodemon 

```
npm run serve
```

### MongoDB

```
//Lancer le server mongo
npm run start-mongo

//Vérifier le statut du server mongo
npm run statusdb

//Arrêter le server mongo
npm run stop-mongo
```

### Mongodb - compass

- Visualiser et administrer sa base de données et ses collections

- [Télécharger et installer compass](https://docs.mongodb.com/compass/master/install) (Linux) :


```
wget https://downloads.mongodb.com/compass/mongodb-compass_1.25.0_amd64.deb
sudo dpkg -i mongodb-compass_1.25.0_amd64.deb
```

- Lancer le logiciel via la terminal : mongodb-compass ou depuis son lanceur d'applications

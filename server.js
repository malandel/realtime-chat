const express = require('express')

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/kittyChat";

const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
let usersArray = [];

app.use(express.static('public'));

// On se sert de socket IO pour emettre / diffuser des événéments
// A la connexion d'un nouvel utilisateur
io.on('connection', (socket) => {
    const userId = socket.id
    console.log(socket.id + ' is connected');
    //Récupérer historique des messages dans la BDD
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("kittyChat");
        dbo.collection("messages").find({}).toArray(function(err, result) {
            if (err) throw err;
            io.emit('connection', result);
            db.close();
        });
    });
    // Sur l'événement 'nouvel utilisateur'
    socket.on('newUser', (username) => {
        usersArray.push({ id: userId, username: username });
        io.emit('newNotification', username + ' vient de se connecter');
        io.emit('updateUsersList', usersArray);
    });
    socket.on('newMessage', (username, msg, date) => {
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("kittyChat");
            var myobj = { user: username, message: msg, date: date };
            dbo.collection("messages").insertOne(myobj, function(err, res) {
                if (err) throw err;
                console.log("1 message inserted");
            });
            db.close();
        });
        io.emit('newMessage', username, msg, date)
    });
    // Tentative avortée de remplacer l'ancien pseudo par le nouveau pseudo dans usersArray
    // socket.on('updatePseudo', (ancienPseudo, newPseudoChanged) => {
    //     console.log(ancienPseudo)
    //     let ancienPseudoArray = usersArray.find(element => element.username == ancienPseudo)
    //         // usersArray.username.replace(ancienPseudoArray, newPseudoChanged)
    //     console.log(usersArray)
    //     io.emit('updateUsersList', usersArray)
    // })
    socket.on('disconnect', () => {
        usersArray = usersArray.filter(user => {
            return user.id != userId
        });
        io.emit('updateUsersList', usersArray);
        console.log(socket.id + ' is disconnected');
    });
});

http.listen(3000, () => {
    console.log('listening on http//:localhost:3000');
});
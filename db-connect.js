const MongoClient = require('mongodb').MongoClient;

const url = "mongodb://localhost:27017/kittyChat";

// Creation database and 'messages' collection
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    let dbo = db.db("kittyChat");
    dbo.createCollection("messages", function(err, res) {
        if (err) throw err;
        console.log("Collection 'messages 'created!");
    });
    console.log("Database 'kittyChat' created!");
    db.close();
});
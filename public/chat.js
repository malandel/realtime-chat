const socket = io()
let username = localStorage.getItem('pseudo')
let avatar = localStorage.getItem('avatar')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
const usersList = document.getElementById('usersList')
const bonjour = document.getElementById('bonjour')
const pseudoForm = document.getElementById('pseudoForm')
const avatarForm = document.getElementById('avatarForm')
const newPseudo = document.getElementById('newPseudo')
const newAvatar = document.getElementById('newAvatar')
const numberUsers = document.getElementById('numberUsers')

// Si l'utlisateur n'existe pas dans le local storage, on en créé un et on le stocke
// socket.on('newUser', function(username) {
while (!username) {
    username = prompt('Quel est votre pseudo ?')
        // while (usersArray.find(user => user.username == username)) {
        //     username = prompt('Pseudo déjà utilisé. Veuillez renseigner un nouveau pseudo')
        // }
    localStorage.setItem('pseudo', username)
}

if (username) {
    bonjour.innerHTML = `Bonjour <strong>${username}</strong> !`
}

// Modifier le pseudo
pseudoForm.onsubmit = function(e) {
    e.preventDefault();
    let ancienPseudo = username
    if (newPseudo.value) {
        localStorage.setItem('pseudo', newPseudo.value)
        newPseudoChanged = localStorage.getItem('pseudo')
        socket.emit('updatePseudo', ancienPseudo, newPseudoChanged)
    }
    newPseudo.value = ''
};
// Ajouter/Modifier son Avatar en le sctockant dans le local storage
avatarForm.addEventListener('submit', function(e) {
    e.preventDefault();
    if (newAvatar.value) {
        localStorage.setItem('avatar', newAvatar.value)
        socket.emit('updateUsersList', avatar)
        newAvatar.value = ''
    }
});

// Envoi du nouvel utilisateur
socket.emit('newUser', username);

// Envoi du message
form.addEventListener('submit', function(e) {
    // on l'empêxhe de recharger la page
    e.preventDefault()
        // S'il y a une valeur
    if (input.value) {
        // On la stocke dans msg
        const msg = input.value;
        // On génère la date selon le serveur
        const date = new Date();
        // On émet l'évènement newMessage
        socket.emit('newMessage', username, msg, date);
        // On efface la valeur de l'input
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    let item = document.createElement('li')
    date = date.slice(0, 10)
    item.innerHTML = `<em>${date}</em> - <strong>${username}</strong> : ${msg}`
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des notifications de connexion
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(usersArray) {
    usersList.textContent = ''
    numberUsers.textContent = ''
    numberUsers.textContent = `(${usersArray.length})`
    usersArray.forEach(user => {
        let item = document.createElement('li')
        item.textContent = user.username
        usersList.appendChild(item)
    });
    console.log(usersArray, 'Users list');
});

// Afficher l'historique des messages archivés à chaque nouvelle connexion
socket.on('connection', function(result) {
    console.log('nouvel événement connection')
    messages.textContent = '';
    console.log(result)
    result.forEach(message => {
        let item = document.createElement('li');
        item.innerHTML = `<em> ${moment(message.date).startOf('second').fromNow()}</em> - <strong>${message.user}</strong> : ${message.message} `;
        messages.appendChild(item);
    });
})